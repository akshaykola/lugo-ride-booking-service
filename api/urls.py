from django.urls import path
from . import views
from django.conf.urls import include
from rest_framework.urlpatterns import format_suffix_patterns

apiurls = [
    path('data/', views.BookingList.as_view(), name='api-data'),
]

urlpatterns = [
    path('', include(apiurls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns = format_suffix_patterns(urlpatterns)

