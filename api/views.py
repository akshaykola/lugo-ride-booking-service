from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import BookingsSerializer
from lugo.models import Bookings


class BookingList(APIView):
    def get(self,request):
        booking1 = Bookings.objects.all()
        serializers = BookingsSerializer(booking1, many=True)
        return Response(serializers.data)

    def post(self, request):
        serializer = BookingsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)