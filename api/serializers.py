from rest_framework import serializers
from lugo.models import Bookings

class BookingsSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Bookings
        fields = ('name', 'contact_no', 'no_of_seats', 'bus_or_train_timings', 'date_of_journey', 'pickup', 'drop', 'fare')