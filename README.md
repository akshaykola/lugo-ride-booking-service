# LUGO - Luggage go #

Lugo is India's first startup that deals with the luggage and goods of the customers. It provides a "Hands-Free Travel" services. These include parcel delivery and temporary luggage storage, which allow travelers to enjoy sightseeing such as shopping, fine dining and other cultural experiences.

It is currently functioning as a Ride Booking Service at NIT Jalandhar.

## Front End :##
### Homepage ###
Homepage contails all the neccesary details about Lugo and its features, offers, contact details etc.

![lugo_index1](screenshots/lugo_index1.JPG)



![lugo_index2](screenshots/lugo_index2.JPG)



![lugo_index3](screenshots/lugo_index3.JPG)

### Booking page ###
Booking page consists of a simple form which asks the required details to make a successful booking. After a successful booking, you are then redirected to the success page.

![bookingform](screenshots/bookingform.JPG)



![success](screenshots/success.JPG)

## Back End :##
A simple RESTful API is created using django_restframework with two methods(GET and POST), which processes the information you send and returns JSON in response.

![post](screenshots/post.JPG)



![get](screenshots/get.JPG)

