from django import forms
from django.contrib.auth.models import User

from .models import Bookings



class BookingForm(forms.ModelForm):

    class Meta:
        model = Bookings
        fields = ['name', 'email_id', 'contact_no', 'no_of_seats', 'bus_or_train_timings', 'date_of_journey', 'pickup', 'drop', 'fare', 'suggestions']

