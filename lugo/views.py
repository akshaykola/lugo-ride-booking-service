from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.views import generic
from django.views.generic import View
from django.http import HttpResponse
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect
from .forms import BookingForm


from .models import Bookings



def index(request):
    return render(request, 'lugo/index.html', {})

class BookingCreate(CreateView):
    model = Bookings
    fields = ['name', 'email_id', 'contact_no', 'no_of_seats', 'bus_or_train_timings', 'date_of_journey', 'pickup',
              'drop', 'fare', 'suggestions']

def success(request):
    return render(request, 'lugo/success.html', {})




