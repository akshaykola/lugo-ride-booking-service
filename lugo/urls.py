from django.urls import path
from . import views
from lugo.models import Bookings
from django.urls import reverse



app_name= 'lugo'

urlpatterns = [
    path('', views.index, name='index'),
    path('book_ride/', views.BookingCreate.as_view(), name='book_ride'),
    path('book_ride/success/', views.success, name='success'),

]
