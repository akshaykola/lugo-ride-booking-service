import datetime
from django.db import models
from django.utils import timezone

# Create your models here.
class Bookings(models.Model):
    PICKUP_CHOICES = [
        ('Hostel 1', 'Hostel 1'),
        ('Hostel 2', 'Hostel 2'),
        ('Hostel 3', 'Hostel 3'),
        ('Hostel 4', 'Hostel 4'),
        ('Hostel 5', 'Hostel 5'),
        ('Hostel 6', 'Hostel 6'),
        ('Hostel 7', 'Hostel 7'),
        ('MBH A', 'MBH A'),
        ('MBH B', 'MBH B'),
        ('MBH F', 'MBH F'),
        ('Mega Girls Hostel', 'Mega Girls Hostel'),
        ('IT Building', 'IT Building'),
    ]

    DROP_CHOICES = [
        ('JRC', 'Jalandhar Cantt(JRC)'),
        ('JUC', 'Jalandhar City(JUC)'),

    ]

    name = models.CharField(max_length=20)
    email_id = models.CharField(max_length=15)
    contact_no = models.CharField(max_length= 12)
    no_of_seats = models.IntegerField()
    bus_or_train_timings = models.CharField(max_length= 15)
    date_of_journey = models.DateField()
    pickup = models.CharField(max_length= 30, choices= PICKUP_CHOICES)
    drop = models.CharField(max_length= 30, choices= DROP_CHOICES)
    fare = models.IntegerField()
    suggestions = models.TextField(max_length=100, blank= True)

    def get_absolute_url(self):
        return 'success/'

    def __str__(self):
        return self.name + ' - ' + self.contact_no + ' - ' + str(self.no_of_seats) + ' - ' + self.bus_or_train_timings + ' - ' + str(self.date_of_journey) + ' - ' + self.pickup + ' - ' + self.drop + ' - ' + str(self.fare)
